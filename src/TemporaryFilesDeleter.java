import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Данный класс представляет собой программу,
 * которая ищёт временные файлы и удаляет их
 *
 * Created by Dasha on 29.03.2018.
 */

class TemporaryFilesDeleter {

    private static Scanner scanner = new Scanner(System.in);
    private static final String[] TEMPORARY_FILES_EXTENSIONS = {"tmp"};
//Это массив с расширениями временных файлов.
// Используется только в режиме сканирования всего компьютера или отдельной папки,
// Не используется в режиме сканирования временной папки.
// Нужен для обеспечения безопасности, чтобы не удалить пользовательские файлы

    public static void main(String[] args) {
        System.out.println("Выберите режим поиска файлов: ");
        System.out.println("1 - поиск и удаление файлов во временной папке\n" +
                "2 - поиск временных файлов по всему компьютеру\n" +
                "3 - поиск временных файлов в определённой папке/диске");
        SearchMode searchMode;//Режим поиска
        switch (scanner.nextInt()) {
            case 1:
                searchMode = SearchMode.ONLY_TEMP_DIR;
                break;
            case 2:
                searchMode = SearchMode.ALL_COMPUTER;
                break;
            case 3:
                searchMode = SearchMode.CUSTOM_PATH;
                break;
            default:
                searchMode = SearchMode.ONLY_TEMP_DIR;
        }
        System.out.println("Введите минимальное количество дней \n от момента последнего изменения файла до текущей даты.");
        System.out.println("Примечание: если файл был изменён 1 явнаря, а сегодня 20 января,\n то до текущей даты прошло 19 дней.");
        int thresholdDaysCount = scanner.nextInt(); //Получаем у пользователя из командной строки количество дней
        ArrayList<File> allGettedFiles = getFilesList(searchMode);
        System.out.println("Поиск временных файлов...");
        ArrayList<File> temporaryFiles = new ArrayList<>();


//Следующий шаг - фильтрация. Нужно из списка всех файлов отобрать только временные.
//Способ фильтрации немного различается для разных режимов.
// Для режима поиска во временной папке проверяется только время последнего изменения,
// а для остальных режимов - ещё и соответствие расширению из списка в начале.
        long allFilesSize = searchTempFilesInList(searchMode, thresholdDaysCount, allGettedFiles, temporaryFiles);

        GetSearchResults(temporaryFiles, allFilesSize);
    }

    /**
     * В этом методе происходит обработка результатов поиска.
     * Если временные файлы были найдены, пользователю предлагается удалить их (или оставить).
     * Иначе выводится сообщение о том, что файлов не было найдено
     * @param temporaryFiles результирующий массив
     * @param allFilesSize размер всех временных файлов
     */
    private static void GetSearchResults(ArrayList<File> temporaryFiles, long allFilesSize) {
        if (temporaryFiles.size() > 0) { //Если хотя бы один временный файл был найден среди всех
            System.out.println("Поиск завершён. Найдено " + temporaryFiles.size() + " временных файлов. Они занимают "
                    + formatFileSize(allFilesSize));
            System.out.println("Вы действительно хотите удалить эти файлы? Для подтверждения введите Y.");
            if (scanner.next().toUpperCase().equals("Y")) {
                System.out.println("Удаление " + temporaryFiles.size() + " файлов...");
                int successfulDeleted = deleteFiles(temporaryFiles);
                System.out.println("Успешно удалено " + successfulDeleted + " временных файлов.");
            } else {
                System.out.println("Удаление отменено пользователем.");
            }
        } else { //Иначе просто сообщаем пользователю, что временных файлов нет
            System.out.println("Поиск завершён. Временных файлов найдено не было.");
        }
    }

    /**
     * Метод, который анализирует список файлов allGettedFiles
     * и выбирает временные файлы соответствующие заданному условию
     *
     * @return Размер всех файлов
     */

    private static long searchTempFilesInList(SearchMode searchMode, int thresholdDaysCount,
                                              ArrayList<File> allGettedFiles, ArrayList<File> temporaryFiles) {
        long allFilesSize = 0;
        for (File currentFile : allGettedFiles) { //Перебираем все найденные в папках файлы
            if (isFileModifiedMoreDaysAgoThan(currentFile, thresholdDaysCount) &&
                    (searchMode == SearchMode.ONLY_TEMP_DIR ||
                            isStrExistsInArray(extractFileExtension(currentFile.getName()), TEMPORARY_FILES_EXTENSIONS))) {
                //А теперь проверяем, были ли файлы из списка изменены более, чем n дней назад, а также соответствуют ли они расширению,
                // если выбран режим поиска файлов по компьютеру или в пользовательской папке.
                temporaryFiles.add(currentFile); // если все условия выполнены, то добавляем в список временных файлов, подлежащих удалению
                allFilesSize += currentFile.length(); //А также добавляем его размер к общему размеру временных файлов,
                // для более наглядной статистики.
                System.out.println("Найден файл \"" + currentFile.getName() + "\". Он был изменён "
                        + howManyDaysAgoFileWasModified(currentFile) + " дней назад и занимает " + formatFileSize(currentFile.length()));
            }
        }
        return allFilesSize;
    }

    /**
     * Метод для корректно удаленных файлов
     *
     * @param temporaryFiles Временные файлы, которые будут удалены
     * @return Колличество всех успешно удаленных файлов
     */

    private static int deleteFiles(ArrayList<File> temporaryFiles) {
        int successfulDeleted = 0; //Счётчик корректно удалённых файлов.
        for (File temporaryFile : temporaryFiles) { //Перебираем все найденные временные файлы
            try {
                if (temporaryFile.delete()) { //Пробуем удалить файл, и смотрим на результат
                    successfulDeleted++; //Если получилось, то увеличиваем счётчик корректно удалённых файлов на единицу
                } else {
                    System.out.println("Проблема во время удаления файла \"" + temporaryFile.getName() + "\".");
                }
            } catch (SecurityException e) {
                System.out.println("Недостаточно прав для удаления файла \"" + temporaryFile.getName() + "\".");
            }
        }
        return successfulDeleted;
    }

    /**
     * Метод для получения всех файлов в выбранной директории и поддиректории
     * @param searchMode Режим поиска, который должен будет выбрать пользователь
     * @return Список всех файлов в виде ArrayList c элементами типа File.
     */

    private static ArrayList<File> getFilesList(SearchMode searchMode) {
        ArrayList<File> allGettedFiles = new ArrayList<>();
        if (searchMode == SearchMode.ONLY_TEMP_DIR) { //Если у нас выбран режим поиска во временной папке
            String tmpDir = System.getProperty("java.io.tmpdir"); //Узнаём у системы путь до папки с временными файлами
            allGettedFiles = getAllFilesFromDirTree(new File(tmpDir)); //Получаем список всех файлов в этой дирректории
        } else if (searchMode == SearchMode.ALL_COMPUTER) { //Если выбран режим поиска по всему компьютеру
            for (String driveletter : getAvailableDrivesList()) { //То мы получаем список разделов
                allGettedFiles.addAll(getAllFilesFromDirTree(new File(driveletter + ':')));
                //И сканируем их всех, добавляя файлы в общий список
            }
        } else { //Если же у нас выбран режим с введением пользовательской дирректории
            String usersPath;
            do {
                System.out.println("Введите путь до дирректории, которую необходимо просканировать.\n" +
                        "Пример: C:, D:\\games и т.д...");
                usersPath = scanner.next();
            } while (!new File(usersPath).exists()); //То мы узнаём у пользователя путь к этой папке
            allGettedFiles.addAll(getAllFilesFromDirTree(new File(usersPath))); //И сканируем её
        }
        return allGettedFiles;
    }

    /**
     * Метод ищет все файлы в дирректории startDir и её поддиректориях рекурсивным способом и возвращает их список
     * @param startDir Начальная дирректория, в которой будет происходить поиск файлов
     * @return Список файлов в виде ArrayList c элементами типа File.
     */
    private static ArrayList<File> getAllFilesFromDirTree(File startDir) {
        ArrayList<File> filesList = new ArrayList<>(); //Создаём список для хранения найденных файлов
        if (startDir.exists() && startDir.isDirectory()) { //Если стартовая дирректория существует и действительно является дирректорией, то
            if (startDir.listFiles() != null) {
                for (File file : startDir.listFiles()) { //Берём список всех файлов и папок внутри неё и перебираем по одному
                    if (file != null) {
                        if (file.isFile()) { //И если очередной файл действительно является файлом, то мы добавляем его в список
                            filesList.add(file);
                        } else {
                            filesList.addAll(getAllFilesFromDirTree(file));
                            //Если же это папка, то мы вызываем этот же метод и добавляем в список результаты его работы.
                        }
                    }
                }
            }
        }
        return filesList; //В конце концов, возвращаем список
    }

    /**
     * Возвращает true, если файл file был изменён более, чем days дней назад
     *
     * @param file Файл, который необходимо проверить
     * @param days Количество дней, после которого файл считается старым
     * @return Булевое значение, равное true, если файл изменён более, чем days дней назад, и false в обратном случае
     */
    private static boolean isFileModifiedMoreDaysAgoThan(File file, int days) {
        return howManyDaysAgoFileWasModified(file) > days;
    }

    /**
     * Возвращает Количество дней от даты последнего изменения файла до текущей даты.
     *
     * @param file Файл, для которого нужно вычислить дату последнего изменения
     * @return Количество дней от даты последнего изменения файла до текущей даты.
     */
    private static int howManyDaysAgoFileWasModified(File file) {
        return (int) ((System.currentTimeMillis() - file.lastModified()) / (24 * 60 * 60 * 1000));
    }

    /**
     * Извлекает расширение из имени файла filename и возвращает его без точки.
     *
     * @param filename Имя файла или путь до него
     * @return Расширение данного файла
     */
    private static String extractFileExtension(String filename) {
        return filename.substring(filename.lastIndexOf('.') + 1);
    }

    /**
     * Метод возвращает список из строк, являющихся
     * буквами разделов, доступных из системы.
     *
     * @return Список разделов в виде букв.
     */
    private static ArrayList<String> getAvailableDrivesList() {
        ArrayList<String> result = new ArrayList<>();
        char driveLetter = 'A';
        do {
            if (new File(driveLetter + ":").exists()) {
                result.add("" + driveLetter);
            }
        } while (driveLetter++ != 'Z');
        return result;
    }

    /**
     * Проверяет, содержит ли массив array хотя бы одно включение строки str.
     *
     * @param str   Строка, для которой нужно вычислить, есть ли её эквивалент в массиве.
     * @param array Массив, в котором будет производиться поиск.
     * @return true, если строка встречается в массиве хотя бы один раз, и false в обратном случае.
     */
    private static boolean isStrExistsInArray(String str, String[] array) {
        for (String currentStr : array) {
            if (currentStr.equals(str)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Возвращает отформатированный размер файла. Пример: 14,23 Kb.
     *
     * @param fileSize Размер файла в байтах
     * @return Отформатированная строка
     */
    private static String formatFileSize(long fileSize) {
        double gettedFileSize = fileSize;
        int prefixSize;
        for (prefixSize = 0; prefixSize < 4; prefixSize++) {
            if (gettedFileSize > 1024) {
                gettedFileSize /= 1024;
            } else {
                break;
            }
        }
        String postfix;
        switch (prefixSize) {
            case 0:
                postfix = "b.";
                break;
            case 1:
                postfix = "Kb.";
                break;
            case 2:
                postfix = "Mb.";
                break;
            case 3:
                postfix = "Gb.";
                break;
            case 4:
                postfix = "Tb.";
                break;
            default:
                postfix = "";
        }
        return String.format("%.2f", gettedFileSize) + " " + postfix;
    }

    /**
     * Класс - enum, содержащий 3 режима для сканирования.
     */
    private enum SearchMode {
        ONLY_TEMP_DIR, ALL_COMPUTER, CUSTOM_PATH
    }
}